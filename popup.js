// Initialize butotn with users's prefered color
let btnCalc = document.getElementById("btnCalc");
let btnDonate = document.getElementById("btnDonate");
let charTrait = 0, charPower = 0;
let weaponTrait = 0, weaponBonusPower = 0;
let weaponStats = new Object();
let enemies = new Object();
let alignedPower = 0, evaluatedAttributeTotal = 0;

const roudingToRoll = 1000;// The rouding to try roll.
const traits = ["fire","earth", "lightning", "water"];
const fireTrait = 0, earthTrait = 1, ligthingTrait = 2, waterTrait = 3, powerTrait = 4; 
const statTexts = ["str", "dex", "cha", "int", "pwr"];
const enemyListTemplate = '<div class="list-item_heading"><h2>-</h2></div><div class="list-item_subheading"><h3>-</h3> </div><div class="list-item_right-content"><h2>-</h2><h3>-</h3></div>';

function calculateEnemies(injectionResults) {
  resetCalculation();

  try{
      if (injectionResults && injectionResults[0] && injectionResults[0].result) 
      {
        let pageHtml = document.createElement("div");
        pageHtml.innerHTML = injectionResults[0].result.replace(/<img /g, '');
        let textBonusPower = document.querySelector("#textBonusPower").value;
        weaponBonusPower = (!isNaN(parseFloat(textBonusPower)) && isFinite(textBonusPower)) 
                          ? parseFloat(textBonusPower): 0;
        generateCharacter(pageHtml);
        generateWeapon(pageHtml);
        generateEnemy(pageHtml);
        
        //set timeout to prevent db click.
        setTimeout(function(){
          let displayBlock = document.querySelector(".display-block");
          displayBlock.style.display = "block";
          displayBlock.classList.remove("disabled");
          displayBlock.removeAttribute("disabled");
          btnCalc.classList.remove("disabled");
          btnCalc.removeAttribute("disabled");
          btnCalc.textContent = "Calculate";
        }, 1000);
      }
    }
    catch(ex)
    {
      alert("If you've found any problem or would like to suggest please contact to: rogeryudya@gmail.com")
      console.error(ex);
    }
}

function generateCharacter(pageHtml) {
  let characterContainer = pageHtml.querySelector(".character-data-column");
  let chracterName = characterContainer.querySelector(".character-name");
  let spanTexts = characterContainer.querySelectorAll(".subtext-stats span");
  let trait = traits.find((el) => { return characterContainer.querySelector("." + el + "-icon") !== null; });

  document.querySelector(".character-name").textContent = chracterName.textContent + " LV." + spanTexts[0].textContent;
  document.querySelector(".character-name").classList.add(trait);
  document.querySelector(".character-power").textContent = "Power: " + spanTexts[1].textContent;
  charTrait = traits.indexOf(trait);
  charPower = parseFloat(spanTexts[1].textContent.replace(",", ""));
}

function generateWeapon(pageHtml) {
  let weaponContainer = pageHtml.querySelector(".weapon-icon-wrapper");
  let trait = traits.find((el) => { return weaponContainer.querySelector("." + el + "-icon") !== null; });
  weaponTrait = traits.indexOf(trait);
  //let weaponPopup = pageHtml.querySelector("#"+weaponTrait.querySelector(".weapon-icon").getAttribute("aria-describedby"));
 // let weaponPopupContent = weaponPopup.querySelector(".tooltip-inner").textContent;
  //document.querySelector(".weapon-star").textContent = ('').padEnd(weaponPopupContent.match(/★/g), '');
//  
  document.querySelector(".weapon-star").classList.add(trait);
  weaponContainer.querySelectorAll(".stats div").forEach((statEl, index) => {
    let statTextIndex = statTexts.findIndex((statText) => { return statEl.querySelector("." + statText) !== null; });
    document.querySelector(".stat-" + (index + 1)).textContent = statEl.children[1].textContent;

    let statValue = parseFloat(statEl.children[1].textContent.replace(statTexts[statTextIndex].toUpperCase(), "").replace("+", "").trim());
    weaponStats[index] = {};
    weaponStats[index].power = statValue;
    weaponStats[index].trait = statTextIndex;
    weaponStats[index].evaluated = formulaAttributePower(statTextIndex, statValue);
    evaluatedAttributeTotal += weaponStats[index].evaluated;

    if (statTextIndex != powerTrait)//pwr
    {
      document.querySelector(".stat-" + (index + 1)).classList.add(traits[statTextIndex]);
    }
  });

  formulaAlignedPower();
}

function generateEnemy(pageHtml) {
  let listContainer = document.querySelector(".list-container");
  let enemiesContainer = pageHtml.querySelectorAll(".content.dark-bg-text .encounter-container");
  enemiesContainer.forEach((e, index) => {
    let trait = traits.find((el) => { return e.querySelector("." + el + "-icon") !== null; });
    let power = parseFloat(e.querySelector(".encounter-power").textContent.replace("Power", "").trim());
    let xp = e.querySelector(".xp-gain").textContent;
    let enemyElement = traits.indexOf(trait);

    let listItem = document.createElement("div");
    listItem.classList.add("list-item");
    listItem.innerHTML = enemyListTemplate;
    let headElement = listItem.querySelector(".list-item_heading h2");
    headElement.classList.add(trait);
    headElement.textContent = trait.charAt(0).toUpperCase() + trait.slice(1);
    enemies[index] = {};
    enemies[index].trait = trait;
    enemies[index].power = power;
    enemies[index].traitBonus = formulaTraitBonus(enemyElement);
    enemies[index].chance = 0;

    listItem.querySelector(".list-item_subheading h3").textContent = "Power : " + power;
    listItem.querySelector(".list-item_right-content h2").classList.add("enemy-chance-"+(index+1));
    listItem.querySelector(".list-item_right-content h2").textContent = transformToPercentage(luckyRoll(power, enemies[index].traitBonus));
    listItem.querySelector(".list-item_right-content h3").textContent = xp;

    listContainer.appendChild(listItem);
  });
}

function resetCalculation()
{
   charTrait = 0, charPower = 0;
   weaponTrait = 0, weaponBonusPower = 0;
   weaponStats =  new Object(), enemies = new Object();
   alignedPower = 0, evaluatedAttributeTotal = 0;
}

//This method just using to determine the roll range based on player power.
function formulaUnalignedPower()
{
  unalignedPower = (((attributeTotal * 0.0025) + 1) * charPower) + weaponBonusPower;
  minimumEnemyPower = Math.floor(unalignedPower * 0.9);
  maximumEnemyPower = Math.floor(unalignedPower * 1.1);
}

function formulaAttributePower(attributeElement, attributeValue)
{
  if(attributeElement != charTrait)
    return attributeValue * 0.0025;
  if(attributeElement == powerTrait)//PWR
    return attributeValue * 0.002575;
  if(attributeElement == charTrait)
    return attributeValue * 0.002675;
}

function formulaTraitBonus(enemyElement)
{
  let traitBonus = 1;

  if(charTrait == weaponTrait)
  {
    traitBonus += 0.075;
  }

  if(charTrait == waterTrait && enemyElement == fireTrait) //water > fire
  {
    traitBonus += 0.075;
  }
  else if(charTrait == fireTrait && enemyElement == waterTrait) //fire > water
  {
     traitBonus -= 0.075;
  }
  else if(charTrait < enemyElement)
  {
    traitBonus += 0.075;
  }
  else if(charTrait > enemyElement)
  {
    traitBonus -= 0.075;
  }

  return traitBonus;
}

function formulaAlignedPower()
{
  alignedPower = ((evaluatedAttributeTotal + 1) * charPower) + weaponBonusPower;
}

function luckyRoll(enemyPower, traitBonus) 
{
  let pPowMin = Math.ceil(alignedPower * 0.9) * traitBonus;
  let pPowMax = Math.floor(alignedPower * 1.1) * traitBonus;
  let ePowMin = Math.ceil(enemyPower * 0.9);
  let ePowMax = Math.floor(enemyPower * 1.1);
  let chance = 0;
  let curr = 1;

  for(; curr <= roudingToRoll; curr++)
  {
    let playerRoll = rand(pPowMin, pPowMax);
    let enemyRoll = rand(ePowMin, ePowMax);

  if(playerRoll >= enemyRoll)
    {
      chance++;
    }
  }

  return chance;
}

function transformToPercentage(value)
{
  return (value / roudingToRoll * 100).toFixed(2) + "%";
}

function rand(t, a)
{
  t = Math.ceil(t);
  a = Math.floor(a);
  return Math.floor(Math.random() * (a - t + 1)) + t;
}

btnDonate.addEventListener('click', () => {
    if(document.querySelector(".popup-donate-information").hasAttribute("hidden"))
      document.querySelector(".popup-donate-information").removeAttribute('hidden'); 
    else
      document.querySelector(".popup-donate-information").setAttribute('hidden','hidden'); 
  });

document.querySelector(".popup-item").addEventListener("click", (e)=>
{
  navigator.clipboard.writeText(e.getAttribute("wallet")).then(function() {
    alert("copied to clipboard");
  }, function() {
    alert("copy failed try again later.");
  });
});

btnCalc.addEventListener("click", async () => {
  let [tab] = await chrome.tabs.query({ active: true, currentWindow: true });

  document.querySelector(".list-container").innerHTML = "";
  let displayBlock = document.querySelector(".display-block");
  displayBlock.style.display = "none";
  displayBlock.classList.add("disabled");
  displayBlock.setAttribute("disabled", "disabled");
  btnCalc.classList.add("disabled");
  btnCalc.setAttribute("disabled", "disabled");
  btnCalc.textContent = "Calculating...";

  chrome.scripting.executeScript({
    target: { tabId: tab.id },
    function: getTabContent
  }, calculateEnemies);
});

function getTabContent() 
{
  return document.querySelector('body').innerHTML;
}